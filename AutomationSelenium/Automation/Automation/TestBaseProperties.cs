﻿using OpenQA.Selenium.Chrome;

namespace Automation
{
    public partial class TestBase
    {
        public ChromeDriver Driver { get; set; }

        public TestBase()
        {
            if (Driver == null)
            {
                Driver = new ChromeDriver();
            }
        }


    }
}
