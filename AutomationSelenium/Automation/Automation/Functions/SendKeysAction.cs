﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Automation.Functions
{

    public static class SendKeysAction
    {
        public static void SendKeys(this IWebDriver driver, string xpath, string text)
        {
            try
            {
                IWebElement element = driver.FindElement(By.XPath(xpath));
                if (element != null)
                    element.SendKeys(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
