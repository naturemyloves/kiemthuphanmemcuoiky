﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Automation.Functions
{

    public static class ClickAction
    {
        public static void Click(this IWebDriver driver, string xpath)
        {
            try
            {
                IWebElement element = driver.FindElement(By.XPath(xpath));
                if (element != null)
                    element.Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
