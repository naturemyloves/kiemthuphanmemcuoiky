﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Automation.TestCase
{
    [TestClass]
    public class Login : TestBase
    {

        [TestMethod]
        public void Execute()
        {
            //after run this line, browser will open
            base.StartUp();
            TC0001();
            TC0002();
            base.CleanUp();
        }

        [TestMethod]
        public void TC0001()
        {
            //step 1. go to url
            //Driver.Url = "Tuoitre.vn";
            Driver.Navigate().GoToUrl("http://Tuoitre.vn");
        }

        [TestMethod]
        public void TC0002()
        {
            //Driver.Url = "Tuoitre.vn";
            Driver.Navigate().GoToUrl("http://Tuoitre.vn");
        }
    }
}
