﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Automation
{
    [TestClass]
    public partial class TestBase 
    {
        [TestInitialize]
        public virtual void StartUp()
        {
            Console.WriteLine("Start Execution");
        }

        [TestCleanup]
        public virtual void CleanUp()
        {
            Driver.Dispose();
            Console.WriteLine("End Execution");
        }
    }
}
