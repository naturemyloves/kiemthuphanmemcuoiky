﻿using Automation.TestCase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            var login = new Login();
            login.Execute();
        }
    }
}
